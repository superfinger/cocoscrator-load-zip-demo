# cocos creator加载与读取zip包


1. 安装`JSZip`插件

下载地址：https://stuk.github.io/jszip/


2. 新建目录`data`，并添加两个文件：

    - a.json
    - Dungeon.png

3. 压缩`data`文件，并修改后缀名为".bin", 将`data.bin`放入项目resources中
4. 加载data.bin文件

    ```jsvascript
     private loadZip(url: string): Promise<any> {
        return new Promise((resolve, reject) => {
            resources.load(url, (err, asset) => {
                if (err) return reject(err);

                assetManager.loadAny({ url: asset.nativeUrl }, null, (err, file) => {
                    if (err) return reject(err);

                    resolve(file);
                })
            })
        });
    }
    ```

5. 读取压缩包里面的文件

    ```
      //解析zip包
        const data = await JSZip.loadAsync(file);
        console.log(data);

        //解析json文件
        data.file("data/a.json").async("text").then((content: string) => {
            this.jsonContentLbl.string = content;
        })

        //解析图片文件
        data.file("data/Dungeon.png").async("base64").then((buf: string) => {
            let img = new Image();
            img.src = 'data:image/png;base64,' + buf;

            let texture = new Texture2D();

            img.onload = () => {
                texture.reset({
                    width: img.width,
                    height: img.height
                })
                texture.uploadData(img, 0, 0);
                texture.loaded = true;

                let spriteFrame = new SpriteFrame();
                spriteFrame.texture = texture;
                this.sp.spriteFrame = spriteFrame;
            }
        });
        ```